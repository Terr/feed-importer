from setuptools import setup
from setuptools.command.test import test as TestCommand


requirements = [
    'requests>=2.13<3.0',
    'chardet==3.0.2',
]

setup(
    name='feed-importer',
    version='0.0.0',
    packages=['feed_importer'],
    install_requires=requirements,
    extras_require={
        'test': ['tox>=2.7'],
        'xml': ['lxml==3.7.3'],
    }
)
