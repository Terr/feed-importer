.PHONY: test test-coverage

test:
	tox

test-coverage:
	tox -- --cov=feed_importer --cov-report=term-missing
