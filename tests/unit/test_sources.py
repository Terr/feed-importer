# -*- coding: utf-8 -*-

import io
import pytest
import requests

from chardet.universaldetector import UniversalDetector
from mock import patch, Mock, MagicMock

from feed_importer import sources


class TestFileSource:
    def test_acknowledges_that_it_can_open_local_paths(self):
        path = '/this/is/an/absolute/path'

        assert sources.FileSource.can_handle_uri(path)

    def test_denies_that_it_can_open_relative_paths(self):
        path = '../this/is/a/relative/path'

        assert not sources.FileSource.can_handle_uri(path)

    def test_iterating_opens_file_and_iterates_through_file_handle(self):
        path = '/test/path'
        source = sources.FileSource(path)

        file_handle = io.TextIOWrapper(io.BytesIO(b'A\nB\n'), encoding='utf8')
        open_ = MagicMock()
        open_.return_value.__enter__.return_value = file_handle

        universal_detector = Mock(spec=UniversalDetector)

        with patch('builtins.open', open_, create=True), \
                patch.object(sources, 'UniversalDetector', universal_detector):
            result = list(source)

        open_.assert_called_with(path, 'r')
        universal_detector.assert_not_called()
        assert ['A\n', 'B\n'] == result

    def test_iterating_through_non_utf8_file_autodetects_encoding(self):
        path = '/test/path'
        source = sources.FileSource(path)

        stream_non_utf8_encoding = io.TextIOWrapper(io.BytesIO(b'\xf6\n' * 20))
        open_ = MagicMock()
        open_.return_value.__enter__.return_value = stream_non_utf8_encoding

        stream_iso8859_encoding = io.TextIOWrapper(io.BytesIO(b'\xf6\n' * 20),
                                                   encoding='iso8859-1')
        io_ = MagicMock()
        io_.open.return_value.__enter__.return_value = stream_iso8859_encoding

        universal_detector = Mock(spec=UniversalDetector)
        universal_detector.return_value.result = {'encoding': 'ISO-8859-1'}

        with patch('builtins.open', open_, create=True), \
                patch.object(sources, 'UniversalDetector', universal_detector), \
                patch.object(sources, 'io', io_):
            result = list(source)

        universal_detector.assert_called_once()
        io_.open.assert_called_once_with(path, encoding='ISO-8859-1')
        assert ['ö\n'] * 20 == result

    def test_mime_type_is_detected_from_filepath(self):
        path = '/test/test.xml'
        source = sources.FileSource(path)

        assert source.get_mime_type() == 'application/xml'


class TestHttpSource:
    # URI, HTTP Content-Type header, expected result
    _url_content_types_data = [
        ['https://some.csv.source/get_feed/?t=123123123', 'text/plain', 'text/plain'],
        ['https://some.xml.source/get_feed/?t=123123123', 'application/xml; charset=UTF-8',
         'application/xml'],
    ]

    def test_acknowledges_that_it_can_handle_http_urls(self):
        path = 'http://some.server/some/path/some.file'

        assert sources.HttpSource.can_handle_uri(path)

    def test_acknowledges_that_it_can_handle_https_urls(self):
        path = 'https://some.server/some/path/some.file'

        assert sources.HttpSource.can_handle_uri(path)

    def test_iterating_opens_url_and_iterates_through_response(self):
        uri = 'http://unittest/path'
        source = sources.HttpSource(uri)
        response = MagicMock(spec=requests.models.Response)
        response.encoding = None
        response.apparent_encoding = 'utf-8'
        response.iter_lines.return_value = ['A', 'B']
        mock_requests = Mock(spec=requests)
        mock_requests.get.return_value = response

        with patch.object(sources, 'requests', mock_requests):
            results = list(source)

        mock_requests.get.assert_called_once_with(uri)
        response.iter_lines.assert_called_once_with(decode_unicode=True)
        assert 'utf-8' == response.encoding
        assert ['A\n', 'B\n'] == results

    @pytest.mark.parametrize('uri,content_type,expected_mime_type', _url_content_types_data)
    def test_mime_type_is_detected_based_on_content_type_header_of_url(self, uri, content_type,
                                                                       expected_mime_type):
        source = sources.HttpSource(uri)

        with patch.object(sources, 'requests') as mock_request:
            mock_request.head.return_value.headers = {'Content-Type': content_type}
            result = source.get_mime_type()

        assert expected_mime_type == result
