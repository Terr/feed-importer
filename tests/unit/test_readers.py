import csv

from io import TextIOBase
from mock import Mock, MagicMock, patch
from xml import sax

from feed_importer import readers


class TestCsvReader:
    def test_iterating_yields_rows_as_dicts(self):
        file_contents = ['Val%d,Val%d,Val%d\n' % (n, n + 1, n + 2) for n in range(0, 30, 3)]
        source_file = MagicMock(spec=TextIOBase)
        source_file.__iter__.return_value = file_contents
        reader = readers.CsvReader(source_file)

        mock_csv_reader = MagicMock()
        mock_csv_reader.__iter__.return_value = [['Val1', 'Val2', 'Val3']]

        mock_csv = MagicMock(spec=csv)
        mock_csv.Sniffer().has_header.return_value = False
        mock_csv.reader.return_value = mock_csv_reader
        with patch.object(readers, 'csv', mock_csv):
            results = list(reader)

        mock_csv.Sniffer().sniff.assert_called_once_with(''.join(file_contents[:10]))
        mock_csv.Sniffer().has_header.assert_called_once_with(''.join(file_contents[:10]))

        assert {'A': 'Val1', 'B': 'Val2', 'C': 'Val3'} == results[0]


class TestXmlReader:
    def test_iterating_yields_rows_as_dicts(self):
        source_file = MagicMock(spec=TextIOBase)
        source_file.__iter__.return_value = [1, 2]
        reader = readers.XmlReader(source_file)

        with patch.object(readers, 'SaxParser') as sax_parser:
            reader.element_complete_callback({'tag1': 'text1'})
            results = list(reader)

        assert 1 == len(results)
        assert {'tag1': 'text1'} == results[0]
        assert 2 == sax_parser.return_value.send.call_count


class TestSaxParser:
    def test_passes_data_to_parser(self):
        parser = Mock(spec=sax.xmlreader.IncrementalParser)
        mock_sax = Mock(spec=sax)
        mock_sax.make_parser.return_value = parser

        with patch.object(readers, 'sax', mock_sax):
            sax_parser = readers.SaxParser(print)
            sax_parser.send('data')

        mock_sax.make_parser.assert_called_once()
        parser.setContentHandler.assert_called_once()
        parser.feed.assert_called_once_with('data')


class TestSimpleContentHandler:
    def test_seeing_end_of_item_element_triggers_callback_with_contents(self):
        callback = Mock()
        content_handler = readers.SimpleContentHandler(callback)

        content_handler.startElement('firstElement', [])
        content_handler.startElement('itemElement', [])
        content_handler.startElement('name', [])
        content_handler.characters('A Name')
        content_handler.endElement('itemElement')
        content_handler.endElement('firstElement')

        callback.assert_called_once_with({'name': 'A Name'})

    def test_seeing_end_of_root_element_does_not_trigger_callback(self):
        callback = Mock()
        content_handler = readers.SimpleContentHandler(callback)

        content_handler.startElement('firstElement', [])
        content_handler.endElement('firstElement')

        callback.assert_not_called()
