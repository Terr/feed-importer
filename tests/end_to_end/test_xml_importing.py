from tests.environment import fixtures_path

from feed_importer import Importer


def test_importing_xml_file_generates_rows():
    xml_importer = Importer(str(fixtures_path / 'small.xml'))

    result = list(xml_importer)

    assert {'name': 'Product A', 'category': 'Clothing', 'price': '42.42'} == result[0]
    assert {'name': 'Product B', 'category': 'Furniture', 'price': '9999.99'} == result[1]


def test_importing_xml_file_with_iso_8859_1_encoding_generates_rows():
    xml_importer = Importer(str(fixtures_path / 'iso8859-1_extended_characters.xml'))

    result = list(xml_importer)

    assert {'name': 'Pröduct A', 'category': 'Clöthing', 'price': '42.42'} == result[0]
    assert {'name': 'Pröduct B', 'category': 'Furniture', 'price': '9999.99'} == result[1]
