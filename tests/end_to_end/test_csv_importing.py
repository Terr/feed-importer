# -*- coding: utf-8 -*-

from tests import http_server

from tests.environment import fixtures_path

from feed_importer import Importer, readers, sources


def test_importing_csv_file_with_header_row_generates_rows():
    csv_importer = Importer(str(fixtures_path / 'with_header_row.csv'))

    results = list(csv_importer)

    assert 10 == len(results)
    assert {'A': 'Product A', 'B': 'Clothing', 'C': '42.42'} == results[0]
    assert {'A': 'Product B', 'B': 'Furniture', 'C': '9999.99'} == results[1]


def test_importing_csv_file_without_header_row_generates_rows():
    csv_importer = Importer(str(fixtures_path / 'without_header_row.csv'))

    results = list(csv_importer)

    assert {'A': 'Product A', 'B': 'Clothing', 'C': '42.42'} == results[0]
    assert {'A': 'Product B', 'B': 'Furniture', 'C': '9999.99'} == results[1]


def test_importing_csv_file_containing_utf8_non_ascii_characters():
    csv_importer = Importer(str(fixtures_path / 'cyrillic_characters.csv'))

    results = list(csv_importer)

    assert 3 == len(results)
    assert {'A': '0', 'B': 'Hello', 'C': 'Здравствуйте'} == results[0]


def test_importing_csv_file_containing_iso8859_1_extended_ascii_characters():
    csv_importer = Importer(str(fixtures_path / 'iso8859-1_extended_characters.csv'))

    results = list(csv_importer)

    assert 2 == len(results)
    assert {'A': '0', 'B': 'Seas', 'C': 'Zeeën'} == results[0]
    assert {'A': '1', 'B': 'Zoology', 'C': 'Zoölogie'} == results[1]


def test_import_csv_file_via_http():
    with http_server.run_http_server() as http_port:
        source = sources.HttpSource('http://localhost:%d/csv' % http_port)
        reader = readers.CsvReader(source)
        results = list(reader)

    assert 2 == len(results)
    assert {'A': 'Product A', 'B': 'Clothing', 'C': '42.42'} == results[0]
    assert {'A': 'Product B', 'B': 'Furniture', 'C': '9999.99'} == results[1]


def test_import_csv_file_with_non_utf8_encoding_via_http():
    with http_server.run_http_server() as http_port:
        source = sources.HttpSource('http://localhost:%d/csv/iso-8859-1' % http_port)
        reader = readers.CsvReader(source)
        results = list(reader)

    assert 2 == len(results)
    assert {'A': '0', 'B': 'Seas', 'C': 'Zeeën'} == results[0]
    assert {'A': '1', 'B': 'Zoology', 'C': 'Zoölogie'} == results[1]
