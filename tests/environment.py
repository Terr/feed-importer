import os

from pathlib import Path


fixtures_path = Path(os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    'fixtures'))
