import io
import socket
import time

from contextlib import contextmanager
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Thread

from tests.environment import fixtures_path


class FileResponseRequestHandler(BaseHTTPRequestHandler):
    STATUS_HTTP_OK = 200

    def do_GET(self):
        self.send_response(self.STATUS_HTTP_OK)
        self.end_headers()

        if self.path == '/csv':
            self._serve_csv_file()
        elif self.path == '/csv/iso-8859-1':
            self._serve_iso_8859_1_csv_file()

    def _serve_csv_file(self):
        with open(str(fixtures_path / 'without_header_row.csv'), 'r') as f:
            for line in f:
                self.wfile.write(line.encode('ascii'))

    def _serve_iso_8859_1_csv_file(self):
        with io.open(str(fixtures_path / 'iso8859-1_extended_characters.csv'),
                     encoding='iso8859-1') as f:
            for line in f:
                self.wfile.write(line.encode('iso8859-1'))


class HttpServer(Thread):
    def __init__(self, port, address='localhost'):
        super().__init__()

        self.http_server = HTTPServer((address, port), FileResponseRequestHandler)
        self.setDaemon(True)

    def run(self):
        self.http_server.serve_forever()

    def shutdown(self):
        self.http_server.shutdown()
        self.http_server.socket.close()


@contextmanager
def run_http_server(port=None):
    if port is None:
        port = get_free_port()

    http_server = HttpServer(port)
    http_server.start()

    try:
        yield port
    finally:
        http_server.shutdown()


def get_free_port():
    s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    address, port = s.getsockname()
    s.close()

    return port


if __name__ == '__main__':
    port = get_free_port()

    with run_http_server() as port:
        print('Serving on port %d. Press Control-C to quit.' % port)

        while True:
            time.sleep(1)
