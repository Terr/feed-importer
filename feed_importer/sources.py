import io
import mimetypes
import requests

from chardet.universaldetector import UniversalDetector
from itertools import islice


class BaseSource:
    def __init__(self, uri):
        self.uri = uri

    @classmethod
    def can_handle_uri(cls, uri):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError

    def get_mime_type(self):
        raise NotImplementedError


class FileSource(BaseSource):
    @classmethod
    def can_handle_uri(cls, uri):
        return uri.startswith('/')

    def __iter__(self):
        try:
            with open(self.uri, 'r') as file_handle:
                yield from file_handle
        except UnicodeDecodeError:
            character_encoding = self._detect_character_encoding()

            with io.open(self.uri, encoding=character_encoding) as file_handle:
                yield from file_handle

    def _detect_character_encoding(self):
        with open(self.uri, 'rb') as file_handle:
            detector = UniversalDetector()
            for line in islice(file_handle, 20):
                detector.feed(line)
            detector.close()

            return detector.result['encoding']

    def get_mime_type(self):
        return mimetypes.guess_type(self.uri)[0]


class HttpSource(BaseSource):
    @classmethod
    def can_handle_uri(cls, uri):
        return uri.startswith('http://') or uri.startswith('https://')

    def __iter__(self):
        response = requests.get(self.uri)

        if response.encoding is None:
            response.encoding = response.apparent_encoding

        for line in response.iter_lines(decode_unicode=True):
            yield line + '\n'

    def get_mime_type(self):
        """Requests Content-Type from URL via HEAD request."""

        response = requests.head(self.uri)
        content_type = response.headers.get('Content-Type', '')

        return content_type.split(';', maxsplit=1)[0]
