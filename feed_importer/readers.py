import csv
import string

from collections import defaultdict
from itertools import islice, chain
from xml import sax


class BaseReader:
    def __init__(self, file_handle):
        self.file_handle = file_handle

    def __iter__(self):
        raise NotImplementedError


class CsvReader(BaseReader):
    def __iter__(self):
        csv_reader = self._prepare_reader()

        for row in csv_reader:
            yield {self._get_column_letter(num): column for num, column in enumerate(row)}

    def _prepare_reader(self):
        iter_file = iter(self.file_handle)
        csv_sample = ''.join(islice(iter_file, 10))
        dialect = csv.Sniffer().sniff(csv_sample)
        has_header = csv.Sniffer().has_header(csv_sample)

        csv_reader = csv.reader(
            chain(csv_sample.splitlines(keepends=True), iter_file),
            dialect=dialect)

        if has_header:
            next(csv_reader)

        return csv_reader

    def _get_column_letter(self, column_number):
        """Converts a 0-indexed (column) number to a string consisting of one or more letters.

        The output is similar to how spreadsheet software label columns.
        """

        quotient, letter_pos = divmod(column_number, 26)
        output = string.ascii_uppercase[letter_pos]

        while quotient:
            quotient, letter_pos = divmod(quotient - 1, 26)
            output = string.ascii_uppercase[letter_pos] + output

        return output


class XmlReader(BaseReader):
    def __init__(self, file_handle):
        super().__init__(file_handle)

        self.element = None

    def __iter__(self):
        xml_parser = self._prepare_parser()

        for data in self.file_handle:
            xml_parser.send(data)

            if self.element:
                yield self.element
                self.element = None

    def _prepare_parser(self):
        return SaxParser(self.element_complete_callback)

    def element_complete_callback(self, element):
        """Callback for a Sax parser to call after it finished reading an element in
        the XML feed.
        """
        self.element = element


class SaxParser:
    def __init__(self, element_complete_callback):
        self.parser = sax.make_parser()
        self.parser.setContentHandler(SimpleContentHandler(element_complete_callback))

    def send(self, data):
        self.parser.feed(data)


class SimpleContentHandler(sax.handler.ContentHandler):
    """Content handler for a XML stream.

    Assumes that the nodes below the root node are the repeated 'items' of the
    feed that you want to retrieve (e.g. <products> -> <product>), and collects its
    children as the properties of that item (e.g. <product> -> <name>, <description>, etc.)
    """

    def __init__(self, element_complete_callback):
        super().__init__()

        self.element_complete_callback = element_complete_callback
        self.root_node_name = None
        self.item_node_name = None
        self.last_name = None

        self._reset_current_node()

    def startElement(self, node_name, attrs):
        self._set_initial_node_trackers(node_name)

        self.last_name = node_name

        if node_name == self.item_node_name:
            self._reset_current_node()
            return

    def _set_initial_node_trackers(self, node_name):
        if self.root_node_name is None:
            self.root_node_name = node_name
        elif self.item_node_name is None:
            self.item_node_name = node_name

    def _reset_current_node(self):
        self.current_node = defaultdict(str)

    def endElement(self, node_name):
        if node_name in self.current_node:
            self.current_node[node_name] = self.current_node[node_name].strip()

        if node_name == self.item_node_name:
            self.element_complete_callback(self.current_node)

    def characters(self, content):
        content = content.strip()

        if content and self.last_name:
            self.current_node[self.last_name] += content
