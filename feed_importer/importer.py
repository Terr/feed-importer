from . import sources, readers, exceptions


class Importer:
    """Helper that attempts to auto detects the type of content of the given URI."""

    sources = [
        sources.FileSource,
        sources.HttpSource,
    ]

    def __init__(self, uri):
        self.uri = uri

    def __iter__(self):
        source = self._get_source()
        if source.get_mime_type() == 'application/xml':
            yield from readers.XmlReader(source)
        else:
            yield from readers.CsvReader(source)

    def _get_source(self):
        for source in self.sources:
            if source.can_handle_uri(self.uri):
                return source(self.uri)

        raise exceptions.NoSuitableReaderFoundError('Could not find a reader that accepts the URI')
