# Feed importer

Simple library for reading data feeds (e.g. CSV, XML) from a local or remote source and to converting the rows into dictionaries.

Everything is done by streaming and/or generating the results, keeping memory usage low even when a data feed is hundreds of MBs big.

## Requirements

* Python 3.4 or higher

## Running tests

In a separate, virtual environment ([venv](https://docs.python.org/3/library/venv.html), Docker, etc.) run:

```
pip install '.[test]'
make test
```

Or, to run the tests with a specific version of Python (e.g. 3.4) as a one-liner:

```docker run --rm -t -v `pwd`:/src python:3.4 bash -c "cd /src && pip install '.[test]' && tox -e py34```
